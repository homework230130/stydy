package com.jinjin.shop.model.service;

import static com.jinjin.shop.common.Template.getSqlSession;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import com.jinjin.shop.model.dto.ProductDTO;
import com.jinjin.shop.model.mapper.ProductMapper;

public class ProductService {

	private ProductMapper productMapper;
	
	public List<ProductDTO> selectAllMenu() {

		SqlSession sqlSession = getSqlSession();
		
		productMapper = sqlSession.getMapper(ProductMapper.class);
		List<ProductDTO> productList = productMapper.selectAllProduct();
		
		sqlSession.close();
		
		return productList;
	}

	public ProductDTO selectMenuByCode(int code) {

		SqlSession sqlSession = getSqlSession();
		productMapper = sqlSession.getMapper(ProductMapper.class);
		
		ProductDTO product = productMapper.selectProductByCode(code);
		
		sqlSession.close();
		
		return product;
	}

	public boolean registProduct(ProductDTO product) {

		SqlSession sqlSession = getSqlSession();
		productMapper = sqlSession.getMapper(ProductMapper.class);

		int result = productMapper.insertProduct(product);
		
		if(result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result > 0 ? true : false;
	}

	public boolean modifyProduct(ProductDTO product) {
		
		SqlSession sqlSession = getSqlSession();
		productMapper = sqlSession.getMapper(ProductMapper.class);
		
		int result = productMapper.updateProduct(product);
		
		if(result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result > 0 ? true : false;
	}
	
	
	public boolean deleteProduct(int code) {
		
		SqlSession sqlSession = getSqlSession();
		productMapper = sqlSession.getMapper(ProductMapper.class);
		
		int result = productMapper.deleteProduct(code);
		
		if(result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result > 0 ? true : false;
	}

	public void searchPrdByRandomPrdCode(List<Integer> randomPrdCodeList) {

		SqlSession sqlSession = getSqlSession();
		productMapper = sqlSession.getMapper(ProductMapper.class);
		
		Map<String, List<Integer>> criteria = new HashMap<>();
		criteria.put("randomPrdCodeList", randomPrdCodeList);
		
		List<ProductDTO> productList = productMapper.searchPrdByRandomPrdCode(criteria);
		
		if(productList != null && !productList.isEmpty()) {
			for(ProductDTO product : productList) {
				System.out.println();
				System.out.println(product);
			}
		} else {
			System.out.println("오늘의 추천상품이 없습니다.");
		}
		
		sqlSession.close();
		
	}

}
