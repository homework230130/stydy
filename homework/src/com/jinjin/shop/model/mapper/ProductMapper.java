package com.jinjin.shop.model.mapper;

import java.util.List;
import java.util.Map;

import com.jinjin.shop.model.dto.ProductDTO;

public interface ProductMapper {

	List<ProductDTO> selectAllProduct();
	
	ProductDTO selectProductByCode(int code);
	
	int insertProduct(ProductDTO product);

	int updateProduct(ProductDTO product);
	
	int deleteProduct(int code);

	List<ProductDTO> searchPrdByRandomPrdCode(Map<String, List<Integer>> criteria);
	
}
