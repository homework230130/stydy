package com.jinjin.shop.model.dto;

public class ProductDTO {

	private int productCode;
	private String productName;
	private int productPrice;
	private int categoryCode;
	private String orderableStatus;
	
	public ProductDTO() {}

	public ProductDTO(int productCode, String productName, int productPrice, int categoryCode, String orderableStatus) {
		super();
		this.productCode = productCode;
		this.productName = productName;
		this.productPrice = productPrice;
		this.categoryCode = categoryCode;
		this.orderableStatus = orderableStatus;
	}

	public int getProductCode() {
		return productCode;
	}

	public void setProductCode(int productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public int getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(int productPrice) {
		this.productPrice = productPrice;
	}

	public int getCategoryCode() {
		return categoryCode;
	}

	public void setCategoryCode(int categoryCode) {
		this.categoryCode = categoryCode;
	}

	public String getOrderableStatus() {
		return orderableStatus;
	}

	public void setOrderableStatus(String orderableStatus) {
		this.orderableStatus = orderableStatus;
	}
	
	@Override 
	public String toString() {
		return "카테고리번호:" + categoryCode + " 상품번호:" + productCode + " 판매가능:" + orderableStatus
				+ "\t상품:" + productName + " "+ productPrice + "원";
	}
}
