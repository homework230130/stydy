package com.jinjin.shop.run;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import com.jinjin.shop.controller.ProductController;
import com.jinjin.shop.model.service.ProductService;

public class Application {

	public static void main(String[] args) {	

		Scanner sc = new Scanner(System.in);
		ProductController productController = new ProductController();
		ProductService productService = new ProductService();
		
		do {
			System.out.println("\n========== Wellcome to jinjin shop! ==========");
			System.out.println("1. 상품 전체 조회");
			System.out.println("2. 상품 번호로 조회");
			System.out.println("3. 새로운 상품 추가");
			System.out.println("4. 상품 수정");
			System.out.println("5. 상품 삭제");
			System.out.println("6. 오늘의 추천 상품 조회");
			System.out.println("==============================================");
			System.out.print("번호를 입력 해주세요 : ");
			int no = sc.nextInt();
			
			switch(no) {
			case 1 : productController.selectAllproduct(); break;
			case 2 : productController.selectProductByCode(inputProductCode()); break;
			case 3 : productController.registMenu(inputProduct()); break;
			case 4 : productController.modifyMenu(inputModifyProduct()); break;
			case 5 : productController.deleteProduct(inputProductCode()); break;
			case 6 : productService.searchPrdByRandomPrdCode(createRandomPrdCodeList()); break;
			default: System.out.println("잘못된 번호를 입력하셨습니다.");
			}
		} while(true);
		
	}

	private static List<Integer> createRandomPrdCodeList() {
		
		Set<Integer> set = new HashSet<>();
		while(set.size() < 3) {
			int temp = ((int) (Math.random() * 30)) + 1;
			set.add(temp);
		}
		
		List<Integer> list = new ArrayList<>(set);
		
		return list;
	}

	private static Map<String, String> inputProductCode() {
		
		Scanner sc = new Scanner(System.in);
		System.out.print("상품 번호 입력 : ");
		String code = sc.nextLine();
		
		Map<String, String> parameter = new HashMap<>();
		parameter.put("code", code);
		
		return parameter;
	}
	
	private static Map<String, String> inputProduct() {
		
		Scanner sc = new Scanner(System.in);
		System.out.print("상품 이름 : ");
		String name = sc.nextLine();
		System.out.print("상품 가격 : ");
		String price = sc.nextLine();
		System.out.print("카테고리 코드 : ");
		String categoryCode = sc.nextLine();
		
		Map<String, String> parameter = new HashMap<>();
		parameter.put("name", name);
		parameter.put("price", price);
		parameter.put("categoryCode", categoryCode);
		
		return parameter;
	}
	
	private static Map<String, String> inputModifyProduct() {
		
		Scanner sc = new Scanner(System.in);
		System.out.print("수정할 상품 번호 : ");
		String code = sc.nextLine();
		System.out.print("수정할 상품 이름 : ");
		String name = sc.nextLine();
		System.out.print("수정할 상품 가격 : ");
		String price = sc.nextLine();
		System.out.print("수정할 카테고리 코드 : ");
		String categoryCode = sc.nextLine();
		
		Map<String, String> parameter = new HashMap<>();
		parameter.put("code", code);
		parameter.put("name", name);
		parameter.put("price", price);
		parameter.put("categoryCode", categoryCode);
		
		return parameter;
	}

}