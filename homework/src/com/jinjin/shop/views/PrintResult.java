package com.jinjin.shop.views;

import java.util.List;

import com.jinjin.shop.model.dto.ProductDTO;

public class PrintResult {

	public void printProductList(List<ProductDTO> productList) {
		for(ProductDTO product : productList) {
			System.out.println(product);
		}
	}
	
	public void printProduct(ProductDTO product) {
		System.out.println(product);
	}

	public void printErrorMessage(String errorCode) {
		String errorMessage = "";
		switch(errorCode) {
		case "selectList" : errorMessage = "상품 목록 조회에 실패하였습니다."; break;
		case "selectOne" : errorMessage = "상품 조회에 실패하였습니다."; break;
		case "insert" : errorMessage = "상품 등록에 실패하였습니다."; break;
		case "update" : errorMessage = "상품 수정에 실패하였습니다."; break;
		case "delete" : errorMessage = "상품 삭제에 실패하였습니다."; break;
		}
		
		System.out.println(errorMessage);
	}

	public void printSuccessMessage(String successCode) {
		String successMessage = "";
		switch(successCode) {
		case "insert" : successMessage = "상품 등록에 성공하였습니다."; break;
		case "update" : successMessage = "상품 수정에 성공하였습니다."; break;
		case "delete" : successMessage = "상품 삭제에 성공하였습니다."; break;
		}
		
		System.out.println(successMessage);
		
	}		
	
}
