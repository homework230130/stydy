package com.jinjin.shop.controller;

import java.util.List;
import java.util.Map;

import com.jinjin.shop.model.dto.ProductDTO;
import com.jinjin.shop.model.service.ProductService;
import com.jinjin.shop.views.PrintResult;

public class ProductController {

	private final ProductService productService;
	private final PrintResult printResult;
	
	public ProductController() {
		productService = new ProductService();
		printResult = new PrintResult();
	}

	public void selectAllproduct() {

		List<ProductDTO> productList = productService.selectAllMenu();
		
		if(productList != null) {
			printResult.printProductList(productList);
		} else {
			printResult.printErrorMessage("selectList");
		}
		
	}

	public void selectProductByCode(Map<String, String> parameter) {
	
		int code = Integer.parseInt(parameter.get("code"));
		
		ProductDTO product = productService.selectMenuByCode(code);
		
		if(product != null) {
			printResult.printProduct(product);
		} else {
			printResult.printErrorMessage("selectOne");
		}
	}
	
	public void registMenu(Map<String, String> parameter) {
		
		ProductDTO product = new ProductDTO();
		product.setProductName(parameter.get("name"));
		product.setProductPrice(Integer.parseInt(parameter.get("price")));
		product.setCategoryCode(Integer.parseInt(parameter.get("categoryCode")));
		
		if(productService.registProduct(product)) {
			printResult.printSuccessMessage("insert");
		} else {
			printResult.printErrorMessage("insert");
		}
	
	}
	
	public void modifyMenu(Map<String, String> parameter) {
		
		ProductDTO product = new ProductDTO();
		product.setProductCode(Integer.parseInt(parameter.get("code")));
		product.setProductName(parameter.get("name"));
		product.setProductPrice(Integer.parseInt(parameter.get("price")));
		product.setCategoryCode(Integer.parseInt(parameter.get("categoryCode")));
		
		if(productService.modifyProduct(product)) {
			printResult.printSuccessMessage("update");
		} else {
			printResult.printErrorMessage("update");
		}
		
	}	
	
	public void deleteProduct(Map<String, String> parameter) {
		
		int code = Integer.parseInt(parameter.get("code"));
		
		if(productService.deleteProduct(code)) {
			printResult.printSuccessMessage("delete");
		} else {
			printResult.printErrorMessage("delete");
		}
		
	}
			
	
}
